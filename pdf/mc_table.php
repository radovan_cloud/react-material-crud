<?php
require("tfpdf/tfpdf.php");
require('db_connect.php');
class PDF_MC_Table extends tFPDF
{
var $widths;
var $aligns;
function Header()
        {

 
if($this->pageNo() > 1){
  // Parameters from UI
   $data = json_decode(file_get_contents('php://input'), true);
   $fields = $data['data'];
   $filedsLength = count($data['data']);
   $maxTableLength = 270;
   $perFiled = $maxTableLength/$filedsLength;  
    //    if($this->pageNo() > 1 and !$this->isFinished1){
        //    $this->Cell(80);
    // $this->SetFillColor(192,192,192);
    // $this->SetTextColor(0,0,0);
    // $this->SetDrawColor(192,192,192);
    // $this->SetLineWidth(.1);
    $this->SetFontSize(10);
    //Header
$header=array('R.B.');
for($i=0;$i<$filedsLength;$i++){
//  array_push($header,$data['data'][i]);
    array_push($header,$data['data'][$i]);
}
$w = array(10);
for($i=0;$i<$filedsLength;$i++){
 array_push($w,$perFiled);
}

  //  $w=array(10,45,45,45);
    for($i=0;$i<count($header);$i++)
        $this->Cell($w[$i],7,$header[$i],1,0,'C',1);
    $this->Ln();
$ko = $this->GetY();    
//$this->setY($ko+2);
}
        }
         function Footer()
    {
        // Go to 1.5 cm from bottom
        $this->SetY(-15);
        // Select Arial italic 8
        $this->SetFont('Arial','I',8);
        // Print centered page number
        $this->Cell(0,10,'Strana '.$this->PageNo(),0,0,'C');
    }
function SetWidths($w)
{
    //Set the array of column widths
    $this->widths=$w;
}

function SetAligns($a)
{
    //Set the array of column alignments
    $this->aligns=$a;
}

function Row($data)
{
    //Calculate the height of the row
    $nb=0;
    for($i=0;$i<count($data);$i++)
        $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
    $h=5*$nb;
    //Issue a page break first if needed
    $this->CheckPageBreak($h);
    //Draw the cells of the row
    for($i=0;$i<count($data);$i++)
    {
        $w=$this->widths[$i];
        $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
        //Save the current position
        $x=$this->GetX();
        $y=$this->GetY();
        //Draw the border
        $this->Rect($x,$y,$w,$h);
        //Print the text
        $this->MultiCell($w,5,$data[$i],0,$a);
        //Put the position to the right of the cell
        $this->SetXY($x+$w,$y);
    }
    //Go to the next line
    $this->Ln($h);
}

function CheckPageBreak($h)
{
    //If the height h would cause an overflow, add a new page immediately
    if($this->GetY()+$h>$this->PageBreakTrigger)
        $this->AddPage($this->CurOrientation);
}

function NbLines($w,$txt)
{
    //Computes the number of lines a MultiCell of width w will take
    $cw=&$this->CurrentFont['cw'];
    if($w==0)
        $w=$this->w-$this->rMargin-$this->x;
    $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
    $s=str_replace("\r",'',$txt);
    $nb=strlen($s);
    if($nb>0 and $s[$nb-1]=="\n")
        $nb--;
    $sep=-1;
    $i=0;
    $j=0;
    $l=0;
    $nl=1;
    while($i<$nb)
    {
        $c=$s[$i];
        if($c=="\n")
        {
            $i++;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
            continue;
        }
        if($c==' ')
            $sep=$i;
        $l+=$cw[$c];
        if($l>$wmax)
        {
            if($sep==-1)
            {
                if($i==$j)
                    $i++;
            }
            else
                $i=$sep+1;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
        }
        else
            $i++;
    }
    return $nl;
}
}
?>