var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var mysql = require("mysql");

// enable CORS
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

// default route
app.get("/", function(req, res) {
  return res.send({ error: true, message: "hello" });
});
// connection configurations
var dbConn = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "users"
});

// connect to database
dbConn.connect();
app.post(`/list`, function(req, res) {
  let table = req.body.table;
  let data = req.body.data;
  let searchField = req.body.search;
  console.log("params", data);
  console.log("searchField", searchField);

  let queryFilter = data.map(item => {
    return `${item} LIKE '%${searchField}%'`;
  });
  console.log("queryFilter:", queryFilter.join(" or "));
  dbConn.query(
    `SELECT * FROM ${table} where ${queryFilter.join(" or ")}`,
    function(error, results, fields) {
      if (error) throw error;
      return res.send({ error: false, data: results, message: "users list." });
    }
  );
});

app.post(`/:table/list`, function(req, res) {
  let table = req.params.table;

  console.log("table listaaaa", table);
  dbConn.query(`SELECT * FROM ${table}`, function(error, results, fields) {
    if (error) throw error;
    return res.send({ error: false, data: results, message: "users list." });
  });
});
// check credetials
app.post(`/users/auth`, function(request, response) {
  let name = request.body.user_name;
  let password = request.body.user_password;
  console.log("name", name);
  console.log("password", password);
  let query = "SELECT * FROM users WHERE name=? AND password=?";
  let params = [name, password];
  console.log("params", params);
  if (name && password) {
    dbConn.query(query, params, function(error, results, fields) {
      console.log(results);
      if (results.length > 0) {
        response.send({ statusMsg: 1, message: "ok" });
      } else {
        response.send({
          statusMsg: 0,
          message: "Invalid username or password"
        });
      }
      response.end();
    });
  } else {
    response.send("Please enter Username and Password!");
    response.end();
  }
});
// Add a new record
app.post(`/insert`, function(req, res) {
  let table = req.body.table;
  let jsonOb = req.body.data;

  let fields = [];
  for (let key in jsonOb) {
    if (jsonOb.hasOwnProperty(key)) {
      fields.push(key);
    }
  }

  function json2array(json) {
    var result = [];
    var keys = Object.keys(json);
    keys.forEach(function(key) {
      result.push("'" + json[key] + "'");
    });
    return result;
  }
  let a = json2array(jsonOb);

  let query = `INSERT INTO ${table} (${fields}) VALUES (${a})`;
  dbConn.query(query, function(error, results, fields) {
    if (error) throw error;
    return res.send({
      error: false,
      data: results,
      status: "1",
      message: "New user has been created successfully."
    });
  });
});

//  Update record
app.put(`/edit`, function(req, res) {
  let id = req.body.id;
  let table = req.body.table;
  let jsonOb = req.body.data;

  let fields = [];
  for (let key in jsonOb) {
    if (jsonOb.hasOwnProperty(key)) {
      fields.push(key);
    }
  }

  function json2array(json) {
    var result = [];
    var keys = Object.keys(json);
    keys.forEach(function(key) {
      result.push("'" + json[key] + "'");
    });
    return result;
  }
  let a = json2array(jsonOb);

  let update_set = Object.keys(jsonOb).map(value => {
    return ` ${value}  = "${jsonOb[value]}"`;
  });
  console.log("update_set-----", update_set);

  let query = `UPDATE ${table} SET ${update_set.join(" ,")} WHERE id = ${id}`;

  dbConn.query(query, function(error, results, fields) {
    if (error) throw error;
    return res.send({
      error: false,
      status: "1",
      message: "user has been updated successfully."
    });
  });
});

//  Delete record
app.delete(`/delete`, function(req, res) {
  let table = req.body.table;
  let id = req.body.id;
  if (!id) {
    return res.status(400).send({ error: true, message: "Please provide id" });
  }
  dbConn.query(`DELETE FROM ${table} WHERE id = ?`, [id], function(
    error,
    results,
    fields
  ) {
    if (error) throw error;
    return res.send({
      error: false,
      data: results,
      message: "User has been deleted successfully."
    });
  });
});

// set port
app.listen(4000, function() {
  console.log("Node app is running on port 4000");
});

module.exports = app;
