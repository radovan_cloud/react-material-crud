import React, { Component } from 'react'
import {decorate, observable, action} from "mobx";
import { RoutedTabs, NavTab } from 'react-router-tabs';
import SpanClose from "../components/SpanClose";

export class Store {

    entity = {};
    search = "";
    Routerx = [<NavTab  key={new Date()} to={'/users'}>Users<SpanClose index={0}/></NavTab>];

    handleChange = (event) =>{
         this.entity[event.target.name] = event.target.value;
    };
    handleChangeSelect = (event) =>{
        console.log("event",event.target);
     //   this.entity[event.target.name] = "{{value: "+event.target.value+", label: "+event.target.name+"}}"
    };
    handleChangeSearch = (event) =>{
        this.search = event.target.value;
    };
    removeTab = (index) =>{

        this.Routerx.splice(index,1);
    }
}

decorate(Store, {
    entity: observable,
    search: observable,
    handleChange: action,
    removeTab: action,
    Routerx: observable
});
