export function fetchPost(url, body) {

    return fetch(url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            "Content-Type": "application/json"
        },
        body: JSON.stringify(body)
    })
        .then((response) => {
            return response.json();
        });
}
export function fetchPut(url, body) {

    return fetch(url, {
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            "Content-Type": "application/json"
        },
        body: JSON.stringify(body)
    })
        .then((response) => {
            return response.json();
        });
}
export function fetchDelete(url, body) {

    return fetch(url, {
        method: 'DELETE',
        headers: {
            'Accept': 'application/json',
            "Content-Type": "application/json"
        },
        body: JSON.stringify(body)
    })
        .then((response) => {
            return response.json();
        });
}
export function fetchGet(url) {

    return fetch(url, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            "Content-Type": "application/json"
        }
    })
        .then((response) => {
            return response.json();
        });
}


