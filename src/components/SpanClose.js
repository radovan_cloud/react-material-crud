import React, { Component } from 'react';
import {observer} from 'mobx-react';
import {store} from "../config/masterSettings";

const styles = {
    close:{
        cursor: 'pointer',
        width: '10px',
        height: '10px',
        paddingLeft: 15
    }

};
const SpanClose = observer(
    class SpanClose extends React.Component {

        spanClick = (index) => {
            store.removeTab(index);
        };
        render(){
            let { index } = this.props;
            return (

                <span onClick={event=>this.spanClick(index)} style={styles.close}>&times;</span>
            )
        }



    });

export default SpanClose;



