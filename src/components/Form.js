import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import {ValidatorForm} from 'react-form-validator-core';
import TextValidator from '../components/TextValidator';
import FormControl from '@material-ui/core/FormControl';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {observer} from 'mobx-react';

import CssBaseline from '@material-ui/core/CssBaseline';
import SelectField from '../components/Select';

const styles = theme => ({
    list: {
        width: 250,
    },
    main: {
        width: 'auto',
        display: 'block', // Fix IE 11 issue.
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
            width: 400,
            marginLeft: 'auto',
            marginRight: 'auto',
        },

    },
    paper: {
      //  marginTop: theme.spacing.unit * 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
      //  backgroundImage: "linear-gradient(to top, #d1f1fb, #dcf0fb, #e6eff9, #edeff4, #f0f0f0);"
    },
    fullList: {
        width: 'auto',
    },
});
const Field = observer((props) => <TextValidator
    fullWidth
    type={'text'}
    onChange={props.store.handleChange}
    name={props.locValue}
    value={props.store.entity[props.locValue]}
    //    validators={(index===0 || index===3)?null:['required']}
    validators={['required']}
    errorMessages={['this field is required']}
/>);
// const SelectField = observer((props) =>
//     <Select
//         name={props.locValue}
//         isClearable={true}
//         value=  {{ value: props.store.entity[props.locValue], label: props.locValue }}
//         onChange={props.store.handleChangeSelect}
//         options={[
//             { value: 'chocolate', label: 'Chocolate' },
//             { value: 1, label: 'author_id' },
//             { value: 'vanilla', label: 'Vanilla' },
//                  ]}
//     />);

const Formgrid = observer((props) => {
    const items = [];

    switch (true){
        case props.cols.length<4:
            props.cols.map((item,index)=> {
                    let locValue = item;
                    items.push(<div>
                        <Grid container spacing={8} key={index}>
                            <Grid item xs={12}>
                                <FormControl margin="normal" fullWidth key={index}>
                                    <label htmlFor="field">
                                        {item}:
                                    </label>

                                  {/*<SelectField store={props.store} locValue={locValue}/>*/}

                                    {/*<Field locValue={locValue} store={props.store}/>*/}


                                    <TextValidator
                                        //  disabled={(index===0 || index===3)}
                                        fullWidth
                                        type={'text'}
                                        onChange={props.store.handleChange}
                                        name={locValue}
                                        value={props.store.entity[locValue]}
                                        //    validators={(index===0 || index===3)?null:['required']}
                                        validators={['required']}
                                        errorMessages={['this field is required']}
                                    />
                                </FormControl>
                            </Grid>
                        </Grid>
                    </div>)
                }
            );

            break;
        case (props.cols.length===4 || props.cols.length===6 || props.cols.length===8) :
            let a = 0;
            let locValue1 = props.cols[a];
            let locValue2 = props.cols[a+1];
            for (let i=1;i<=props.cols.length/2;i++) {
                items.push(

                    <div>
                        <Grid container spacing={8} key={i}>
                            <Grid item xs>
                                <FormControl margin="normal" fullWidth key={i}>
                                    <label htmlFor="field">
                                        {locValue1}:
                                    </label>
                                    <Field locValue={locValue1} store={props.store}/>
                                </FormControl>
                            </Grid>
                            <Grid item xs>
                                <FormControl margin="normal" fullWidth key={i+1}>
                                    <label htmlFor="field">
                                        {locValue2}:
                                    </label>
                                    <Field locValue={locValue2} store={props.store}/>
                                </FormControl>
                            </Grid>
                        </Grid>
                    </div>
                );
                a=a+2;
            }
            break;
        case (props.cols.length % 2 === 1 && props.cols.length > 4):
            let locValue3 = props.cols[0];
            items.push(
                <Grid container spacing={8} key={100}>
                    <Grid item xs={12}>
                        <FormControl margin="normal" fullWidth key={0}>
                            <label htmlFor="field">
                                {locValue3}:
                            </label>
                            <Field locValue={locValue3} store={props.store}/>
                        </FormControl>
                    </Grid>
                </Grid>
            );
            let a_ = 1;
            let locValue4 = props.cols[a_];
            let locValue5 = props.cols[a_+1];
            for (let i=1;i<=props.cols.length/2;i++) {
                items.push(

                    <div>
                        <Grid container spacing={8} key={i+1}>
                            <Grid item xs>
                                <FormControl margin="normal" fullWidth key={i}>
                                    <label htmlFor="field">
                                        {locValue4}:
                                    </label>
                                    <Field locValue={locValue4} store={props.store}/>
                                </FormControl>
                            </Grid>
                            <Grid item xs>
                                <FormControl margin="normal" fullWidth key={i+1}>
                                    <label htmlFor="field">
                                        {locValue5}:
                                    </label>
                                    <Field locValue={locValue5} store={props.store}/>
                                </FormControl>
                            </Grid>
                        </Grid>
                    </div>
                );
                a_=a_+2;
            }
            break;
        default:
            break;
    }

    return items });
const TemporaryDrawer = observer(
    class TemporaryDrawer extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
            };
        }
        handleSubmit = () => {
            (this.props.store.entity.isEditing) ? this.props.editRecord() : this.props.addNewRecord()
        };

        render() {
            const { classes, fields, store,editFields} = this.props;
            console.log('fileds',fields);
            return (
                <div >

                                        <Typography component="h1" variant="h5">
                                            Dodaj / izmijeni :
                                        </Typography>
                                        <ValidatorForm
                                            ref="form"
                                            onSubmit={this.handleSubmit}
                                          //  className={'container'}
                                        >
                                            <Formgrid cols={editFields} store={store}/>
                                            {/*{editFields.map((item,index)=>(*/}
                                                {/*<FormControl margin="normal" fullWidth key={index}>*/}
                                                    {/*<label htmlFor="username">*/}
                                                        {/*{item}:*/}
                                                    {/*</label>*/}
                                                    {/*<TextValidator*/}
                                                      {/*//  disabled={(index===0 || index===3)}*/}
                                                        {/*fullWidth*/}
                                                        {/*type={'text'}*/}
                                                        {/*onChange={store.handleChange}*/}
                                                        {/*name={item}*/}
                                                        {/*value={store.entity[item]}*/}
                                                    {/*//    validators={(index===0 || index===3)?null:['required']}*/}
                                                        {/*validators={['required']}*/}
                                                        {/*errorMessages={['this field is required']}*/}
                                                    {/*/>*/}
                                                {/*</FormControl>*/}
                                            {/*))}*/}
                                            <div style={{color: 'red'}}>
                                                {store.entity.error}
                                            </div>
                                            <Button
                                                type="submit"
                                                fullWidth
                                                variant="contained"
                                                color="primary"
                                                className={classes.submit}
                                                style={{marginTop: 30}}
                                            >
                                                Submit
                                            </Button>
                                        </ValidatorForm>

                </div>
            );
        }
    }
);

TemporaryDrawer.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TemporaryDrawer);
