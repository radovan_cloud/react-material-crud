import React, { Component } from 'react'
import Select from 'react-select'
import {observer} from 'mobx-react';

const options = [
    { value: 1, label: 'Adell Bailey6' },
    { value: 3, label: 'Cecelia Cronin' },
    { value: 4, label: 'Sallie Hand' }
];

const SelectField = observer(
    class SelectField extends React.Component {
        state = {
            selectedOption: { value: 3, label: 'Cecelia Cronin' },
                    };
        handleChange = selectedOption => {
            this.setState({ selectedOption });
            this.props.store.entity[this.props.locValue] = selectedOption;
            console.log(`Option selected:`, selectedOption);
        };
        render(){
            let { selectedOption } = this.state;
            return (
                <Select
                    value={this.props.store.entity[this.props.locValue]}
                    onChange={this.handleChange}
                    options={options} />
            )
        }



    });

export default SelectField;