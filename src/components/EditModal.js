import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import {observer} from 'mobx-react';

const styles = theme => ({
    list: {
        width: 450,
    },
    fullList: {
        width: 'auto',
    },
    center: {
        marginTop: theme.spacing.unit * 4,
//        display: 'flex',
 //       flexDirection: 'column',
        marginLeft: 300,
        marginRight: 300,
        alignItems: 'center',
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
});
const TemporaryDrawer = observer(
class TemporaryDrawer extends React.Component {

    render() {
        const {toggleDrawer, classes} = this.props;
      return (
            <div>
                <Drawer anchor="top" open={this.props.top} onClose={toggleDrawer}>
                    <div
                        tabIndex={0}
                        role="button"
                     //   onClick={toggleDrawer}
                     //   onKeyDown={toggleDrawer}
                    >
                    <div style={{height: 700}}>
                        <div className={classes.center}>
                            {this.props.Form}
                    </div>
                    </div>
                    </div>
                </Drawer>
            </div>
        );
    }
}
);

TemporaryDrawer.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TemporaryDrawer);
