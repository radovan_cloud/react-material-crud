import React from 'react';
import { ValidatorComponent } from 'react-form-validator-core';
import Input from '@material-ui/core/Input';
class TextValidator extends ValidatorComponent {

    render() {
        const { errorMessages, validators, requiredError, validatorListener, ...rest } = this.props;

        return (
            <div>

                <Input
                    {...rest}
                    ref={(r) => { this.input = r; }}
                    placeholder={ this.props.hint }
                    type= { this.props.type }
                />
                {this.errorText()}
            </div>
        );
    }

    errorText() {
        const { isValid } = this.state;

        if (isValid) {
            return null;
        }

        return (
            <div style={{ color: 'red' }}>
                {this.getErrorMessage()}
            </div>
        );
    }
}

export default TextValidator;