import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import {withRouter} from "react-router-dom";
import {tableNameasArray,tables} from "../config/masterSettings";
import {store} from "../config/masterSettings";
import {observer} from 'mobx-react';
import {toJS} from "mobx";
import { RoutedTabs, NavTab } from 'react-router-tabs';
import SpanClose from "../components/SpanClose";

const styles = {
    list: {
        width: 250,
    },
    fullList: {
        width: 'auto',
    },
    close:{
        cursor: 'pointer',
        width: '10px',
        height: '10px',
        paddingLeft: 15
}

};
const TemporaryDrawer = observer(
class TemporaryDrawer extends React.Component {

    onClick = (item) => {

        let curIndex = store.Routerx.length;
        store.Routerx.push(
            <NavTab key={new Date()} to={'/'+item}>{item.charAt(0).toUpperCase() + item.slice(1)}<SpanClose index={curIndex}/></NavTab>
        );
        console.log("tab**********",toJS(store.Routerx));
        this.props.history.push('/'+item)
    };
    render() {
        const { classes } = this.props;

        const sideList = (
            <div className={classes.list}>
                <List>
                    {tableNameasArray().map(item=>

                    <ListItem button key={item} onClick={()=> this.onClick(item)}>
                        <ListItemIcon>{<InboxIcon /> }</ListItemIcon>
                        <ListItemText primary={tables[item].title} />
                    </ListItem>
                    )}

                </List>
            </div>
        );

        return (
            <div>
                <Drawer open={this.props.menuOpen} onClose={this.props.handleMenuOpen}>
                    <div
                        tabIndex={0}
                        role="button"
                        onClick={this.props.handleMenuOpen}
                        onKeyDown={this.props.handleMenuOpen}
                    >
                        {sideList}
                    </div>
                </Drawer>

            </div>
        );
    }
});

TemporaryDrawer.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(withRouter(TemporaryDrawer));