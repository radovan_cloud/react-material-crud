import React from 'react';
import AppBar from '../components/AppBar';
import Master from '../pages/Master';
import {withRouter} from "react-router-dom";

class FirstPageWrapper extends React.Component {

    render() {
        return (<div >
            <AppBar/>
                {this.props.ta}
            <Master />
                </div>
        );
    }

}

export default (withRouter(FirstPageWrapper));