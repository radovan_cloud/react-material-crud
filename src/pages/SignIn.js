import React from 'react';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import {withRouter} from "react-router-dom";
import {ValidatorForm} from 'react-form-validator-core';
import TextValidator from '../components/TextValidator';
import {store,usersTable} from "../config/masterSettings";
import {observer} from 'mobx-react';

const styles = theme => ({
    main: {
        width: 'auto',
        display: 'block', // Fix IE 11 issue.
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
            width: 400,
            marginLeft: 'auto',
            marginRight: 'auto',
        },

    },
    paper: {
        marginTop: theme.spacing.unit * 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
        backgroundImage: "linear-gradient(to top, #d1f1fb, #dcf0fb, #e6eff9, #edeff4, #f0f0f0);"
    },
    avatar: {
        margin: theme.spacing.unit,
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing.unit,
    },
    submit: {
        marginTop: theme.spacing.unit * 3,
    },
});

const SignIn = observer(
    class SignIn extends React.Component {
        componentWillMount(){
            // init create entirty fields
            store.entity['Name'] = '';
            store.entity['Password'] = '';
        }
        handleSubmit = () => {
            let params = {
                user_name: store.entity.Name,
                user_password: store.entity.Password,
            };
            let API_route = 'auth';
            let url = "http://localhost:4000/"+usersTable+"/"+API_route;
            return fetch(url, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(params)
            })
                .then((response) => {
                    return response.json();
                })
                .then(data => {
                    if (data.statusMsg === 1) {
                        store.entity['isLogged'] = true;
                        this.props.history.push('/'+usersTable);

                    } else {
                        store.entity['message'] = data.message;
                    }
                    return data

                })
                .catch(error => {
                    return error;
                });
        };

        error = () => {
            return (
                <div style={{color: 'red'}}>
                    {store.entity.message}
                </div>
            )
        };

        render() {
            const {classes} = this.props;

            let Name = store.entity.Name;

            let Password = store.entity.Password;
            return (
                <main className={classes.main}>
                    <CssBaseline/>
                    <Paper className={classes.paper}>
                        <Avatar className={classes.avatar}>
                            <LockOutlinedIcon/>
                        </Avatar>
                        <Typography component="h1" variant="h5">
                            Sign in
                        </Typography>
                        <ValidatorForm
                            ref="form"
                            onSubmit={this.handleSubmit}
                            className={'container'}
                        >
                            <FormControl margin="normal" fullWidth>
                                <label htmlFor="username">
                                    Name:
                                </label>
                                <TextValidator
                                    autoFocus
                                    fullWidth
                                    type={'text'}
                                    onChange={store.handleChange}
                                    name="Name"
                                    value={Name}
                                    validators={['required']}
                                    errorMessages={['this field is required']}
                                />
                            </FormControl>
                            <FormControl margin="normal" fullWidth>
                                <label htmlFor="password">
                                    Password:
                                </label>
                                <TextValidator
                                    fullWidth
                                    type={'password'}
                                    onChange={store.handleChange}
                                    name="Password"
                                    value={Password}
                                    validators={['required']}
                                    errorMessages={['this field is required']}
                                />
                            </FormControl>
                            {this.error()}
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}
                            >
                                Sign in
                            </Button>
                        </ValidatorForm>
                    </Paper>
                </main>
            );
        }
    }
)

SignIn.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(withRouter(SignIn));