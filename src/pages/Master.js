import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import {store,editColumns,tableColumns,tableHeaderTitles,tableHeaderLinks,searchColumns,tables,appTitle} from "../config/masterSettings";
import {observer} from 'mobx-react';
import EditModal from "../components/EditModal";
import Form from "../components/Form";
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import Typography from '@material-ui/core/Typography';
import Searchbar from '../components/SearchBar';
import {withRouter} from 'react-router-dom';
import TableEmpty from "../components/Table";
import Button from '@material-ui/core/Button';
import { ExportCSV } from '../components/ExportReactCSV';

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
    fab: {
        position: 'fixed',
        top: theme.spacing.unit * 12,
        right: theme.spacing.unit * 2,
    },
    row: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.background.default,
        },
    }
});

const TableData = observer(
    class TableData extends React.Component {
        state = {

            rowdata: [],
            id: '',
            top: false,
            heather: [],
            editFields: [],
            searchFields: [],
            headers: [],
            table: '',
            tableHeaders: [],
            tableLinks: [],
            title: ''

        };
        componentDidMount() {
            console.log('did');
            console.log('tables.title',tables[this.props.location.pathname.slice(1)].title);
            this.setState({table:this.props.location.pathname.slice(1),title: tables[this.props.location.pathname.slice(1)].title});
            this.getAll();
        }
        createPdf = () =>{
            let params = {
                appTitle: appTitle,
                title: tables[this.props.location.pathname.slice(1)].title,
                table: this.props.location.pathname.slice(1),
                search: store.search,
                data: [],
                dataSearch: []
            };
            this.state.headers.map(item => (
                params.data.push(item)
            ));
            this.state.searchFields.map(item => (
                params.dataSearch.push(item)
            ));
           // let API_route = 'report';
            let url = "http://localhost/pdf/pdf1.php";
            return fetch(url, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(params)
            })
                .then((response) => {
                    window.open('http://localhost/pdf/report.pdf');
                    return response.json();
                })
                .then(data => {
                })
                .catch(error => {
                    return error;
                });
        };

        search = () => {
            this.getAllPost();
        };
        keyPressed = (event) => {
            if (event.key === "Enter") {
                this.getAllPost();
            }
        };
        toggleDrawer =  (row) => {
            this.setState(prevState => ({
                top: !prevState.top,
            }));
        };
        closeEditModal =  () => {
         //   this.getAll();
            this.setState({top: false});
        };
        getAllPost = () => {
            console.log('getting list filtered');
            let params = {
                table: this.props.location.pathname.slice(1),
                search: store.search,
                tableHeaders: this.state.tableHeaders,
                HeaderLinks: this.state.tableLinks,
                data: []
            };
            this.state.searchFields.map(item => (
                params.data.push(item)
            ));
            let API_route = 'list';
            let url = "http://localhost:4000/"+API_route;
            return fetch(url, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(params)
            })
                .then((response) => {
                    return response.json();
                })
                .then(data => {
                    console.log('lista--->',data.data);
                     this.setState({rowdata: data.data});
                })
                .catch(error => {
                    return error;
                });
        };
        getAll = () => {

            let params = {
             //   table: this.props.location.pathname.slice(1),
             //   search: store.search,
                tableHeaders: tableColumns(this.props.location.pathname.slice(1)),
                HeaderLinks: tableHeaderLinks(this.props.location.pathname.slice(1)),
             //   data: []
            };
            console.log("Params--------",params);
            let API_route = 'list';
           // let srch = (store.search==='' ? 'all':store.search);
         //   console.log('srch',srch);
            let url = "http://localhost:4000"+this.props.location.pathname+"/"+API_route;
            return fetch(url, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(params)
            })
                .then((response) => {
                    return response.json();
                })
                .then(data => {
                    console.log('users--->',data.data);

                    let jsonOb = data.data[0];
                    console.log('jsonOb',jsonOb);
                    // all table headers from current table from db
                    let heather = [];
                    for (let key in jsonOb) {
                        if (jsonOb.hasOwnProperty(key)) {
                            heather.push(key);
                        }
                    }
                    console.log("lista polja iz db",heather);
                    let editColums = editColumns(this.props.location.pathname.slice(1));
                    let tableHeaders = tableColumns(this.props.location.pathname.slice(1));
                    let HeaderTitles = tableHeaderTitles(this.props.location.pathname.slice(1));
                    let HeaderLinks = tableHeaderLinks(this.props.location.pathname.slice(1));
                    console.log('tableHeaders++++',tableHeaders);
                    console.log('edit fileds***',editColums);
                    let HEADERS = []; // all filed names from database that match tableView defined in masterSettings
                    heather.map((item,index)=>{
                        if (tableHeaders.indexOf(item) !== -1) {
                            HEADERS.push(item);
                        }
                    });
                    console.log('HEADERS++++',HEADERS);
                    let EDITFIELDS = [];
                    heather.map((item,index)=>{
                       if (editColums.indexOf(item) !== -1) {
                           EDITFIELDS.push(item);
                       }

                    });
                    let searchColumns1 = searchColumns(this.props.location.pathname.slice(1));
                    let SEARCHFIELDS = [];
                    heather.map((item,index)=>{
                        if (searchColumns1.indexOf(item) !== -1) {
                            SEARCHFIELDS.push(item);
                        }

                    });
                    this.setState({rowdata: data.data,heather:heather,tableLinks:HeaderLinks,editFields:editColums,headers: HeaderTitles,tableHeaders:tableHeaders,searchFields:SEARCHFIELDS});
                })
                .catch(error => {
                    return error;
                });
        };
        deleteRecord = (row) =>{
            let params = {
                id: row.id,
                table: this.props.location.pathname.slice(1)
            };
            let API_route = 'delete';
            let url = "http://localhost:4000/"+API_route;
            return fetch(url, {
                method: 'DELETE',
                headers: {
                    'Accept': 'application/json',
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(params)
            })
                .then((response) => {
                    return response.json();
                })
                 .then(data => {
                     if (data.error === false) {
                         // update table - fetch users
                         this.getAll();
                     } else {
                         store.entity.message = data.message;
                     }
                     return data

                 })
                 .catch(error => {
                     return error;
                 });
        };
        editRecordModal = (row) =>{
            store.entity['isEditing'] = true;
            console.log('logg******');
            this.state.heather.map(item=>(
                store.entity[item] =  row[item]
            ));
            console.log('row on open modal',row);
            this.toggleDrawer(row);


        };
        deleteIcon = (row) => {
            return (
             <IconButton onClick={() => this.deleteRecord(row)}>
                <DeleteIcon color="primary" />
             </IconButton>
            )
    };
        editIcon = (row) => {
            return (
                <IconButton onClick={() => this.editRecordModal(row)}>
                    <EditIcon color="primary" />
                </IconButton>
            )
        };
        addNew = () =>{
            store.entity.isEditing = false;
            this.state.editFields.map(item=>(
                store.entity[item] = ''
            ));
            console.log('store.entity',store.entity);
            console.log('parms<>',this.state.editFields);
            this.setState({ top: true});
        };
        addNewRecord = async() => {
            let params = {
                table: this.props.location.pathname.slice(1),
                data: {}
            };

            await this.state.editFields.map(item => {
                params.data[item]= store.entity[item];

            });
            let API_route = 'insert';
            let url = "http://localhost:4000/"+API_route;
            return fetch(url, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(params)
            })
                .then((response) => {
                    return response.json();
                })
                .then(data => {
                            if(data.status==='1'){
                                this.closeEditModal();
                                this.getAll();
                                store.entity.error = '';
                            } else {
                                store.entity.error = data.message;

                            }
                    return data

                })
                .catch(error => {
                    return error;
                });
        };

        editRecord = () => {
            console.log('ulaz u funkciju editRecord');

            let params = {
                table: this.props.location.pathname.slice(1),
                id: store.entity.id,
                data: {}
            };
             this.state.editFields.map(item => (
                params.data[item]= store.entity[item]
            ));
            console.log('params fro store',params);
            let API_route = 'edit';
            let url = "http://localhost:4000/"+API_route;
            return fetch(url, {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(params)
            })
                .then((response) => {
                    return response.json();
                })
                .then((data) => {
                console.log('resolve');
                    if(data.status==='1'){
                        this.closeEditModal();
                        this.getAll();
                        store.entity.error = '';

                    } else {
                        store.entity.error = data.message;
                    }

                })
                .catch(error => {
                    return error;
                });
        };
    render(){
        const { rowdata,top,heather,table,editFields,headers,tableHeaders,title} = this.state;

   let arr = (row) => {
       const items = [];
       for (const [index, value] of this.state.heather.entries()) {
           if(tableHeaders.indexOf(index) !== -1) {
               items.push(
                   <TableCell key={index} align="left">{row[value]}</TableCell>
               );
           }
       }
       return items
   };

        const { classes} = this.props;
        const form = () => <Form addNewRecord={this.addNewRecord} store={store} editFields={editFields} fields={heather} editRecord={this.editRecord}/>;
    return (<div style={{marginTop: 50,padding:40}}>
            <Fab color="primary" aria-label="Add" className={classes.fab} onClick={this.addNew}>
                <AddIcon />
            </Fab>
            <Typography variant="h6" gutterBottom  padding={10}>
                {title}
            </Typography>
        <div style={{ display: 'inline-flex' }}>
            <Searchbar search={this.search} keyPressed={this.keyPressed} autoFocus/>
            <Button variant="contained" className={classes.button} style={{marginLeft: 30}} onClick={this.createPdf}>
                PDF report
            </Button>
            <ExportCSV csvData={this.state.rowdata} fileName={'fajlExcell'} />
        </div>
        <Paper className={classes.root}>
            <EditModal toggleDrawer={this.toggleDrawer} top={top} Form={form()}  addNewRecord={this.addNewRecord} editRecord={this.editRecord} close={this.closeEditModal}/>
            <TableEmpty headers={headers} rowdata={rowdata} tableHeaders={tableHeaders} heather={heather} editIcon={this.editIcon} deleteIcon={this.deleteIcon} />
            {/*<Table className={classes.table}>*/}
                {/*<TableHead>*/}
                    {/*<TableRow>*/}
                        {/*{headers.map((row,index) => (*/}
                            {/*<TableCell key={index}>{row}</TableCell>*/}
                        {/*))}*/}
                        {/*<TableCell align="right">Action</TableCell>*/}
                    {/*</TableRow>*/}
                {/*</TableHead>*/}
                {/*<TableBody>*/}
                    {/*{rowdata.map((row,index) => (*/}
                        {/*<TableRow key={index} className={classes.row}>*/}
                            {/*{arr(row)}*/}
                            {/*<TableCell align="right">{this.editIcon(row)}{this.deleteIcon(row)}</TableCell>*/}
                        {/*</TableRow>*/}
                    {/*))}*/}
                {/*</TableBody>*/}
            {/*</Table>*/}

        </Paper>
        </div>
    )
}
});

TableData.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles, { withTheme: true })(TableData));
