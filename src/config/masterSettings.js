import {Store} from "../Store/store";

export const appTitle = "Test app";
export const store = new Store();
export const usersTable = 'users';
export const tables = {'users':   {
                                   title: 'Korisnici',
                                   edit:
                                       ["name","password"],
                                   tableView:
                                       [
                                           {
                                               dbName:"name",
                                               title:"Ime"
                                           },
                                           {
                                               dbName:"password",
                                               title:"Lozinka"
                                           },
                                           {
                                               dbName:"dob",
                                               title:"Kreiran"
                                           }
                                       ],
                                    //   ["name","password","created"],
                                   search:
                                       ["name","dob"]
                                  },
                       // 'authors': {
                       //             title: 'Autori',
                       //             edit:
                       //                 ["first_name","last_name","email"],
                       //             tableView:
                       //                 [
                       //                     {
                       //                         dbName:"first_name",
                       //                         title:"Ime"
                       //                     },
                       //                     {
                       //                         dbName:"last_name",
                       //                         title:"Prezime"
                       //                     },
                       //                     {
                       //                         dbName:"email",
                       //                         title:"Email"
                       //                     }
                       //                 ],
                       //              //  ["first_name","last_name","email"],
                       //             search:
                       //                 ["first_name"]
                       //            },
                       // 'posts':   {
                       //             title: 'Postovi',
                       //             edit:
                       //                 ["author_id","title"],
                       //             tableView:
                       //             [
                       //                 {
                       //                 dbName:"authors.first_name",
                       //                 title:"Ime autora",
                       //                 link: ["join authors on authors.id=posts.author_id","author_id"]
                       //                 },
                       //                 {
                       //                 dbName:"authors.last_name",
                       //                 title:"Prezime autora",
                       //                 },
                       //                 {
                       //                 dbName:"title",
                       //                 title:"Naslov"
                       //                 },
                       //                 ],
                       //               //  ["author_id","title"],
                       //             search:
                       //                 ["title"]
                       //            }
};

export const tableNameasArray = ()=>{
    let tables_ = tables;
    let tableNames = [];
    for (let key in tables_) {
        if (tables_.hasOwnProperty(key)) {
            tableNames.push(key);
        }
    }
    return tableNames;
};

export const editColumns = (table)=>{
    let tables_ = tables;
    for (let key in tables_) {
        if (tables_.hasOwnProperty(key)) {
            if(table===key)
                return (tables_[key]['edit']);
        }
    }
};

export const tableColumns = (table)=>{
    let tables_ = tables;
    let arr = []; // array of tableView fields (dbName and title)
    let final = []; // final array to be returned (contain only dbName's)
    for (let key in tables_) {
        if (tables_.hasOwnProperty(key)) {
            if(table===key) {
                arr=(tables_[key]['tableView']);
            }
        }

    }
    let i=0;
    for (let key in arr) {
        if (arr.hasOwnProperty(key)) {
            let str = arr[i]['dbName'];
            if(str.indexOf('.') !== -1){
                str = str.substring(str.indexOf(".") + 1 );
                console.log("---------",str);
            }
            final.push(str);
            i++;
        }

    }
    console.log("final",final);
    return final
};
export const tableHeaderTitles = (table)=>{
    let tables_ = tables;
    let arr = []; // array of tableView fields (dbName and title)
    let final = []; // final array to be returned (contain only dbName's)
    for (let key in tables_) {
        if (tables_.hasOwnProperty(key)) {
            if(table===key) {
                arr=(tables_[key]['tableView']);
            }
        }

    }
    let i=0;
    for (let key in arr) {
        if (arr.hasOwnProperty(key)) {

            final.push(arr[i]['title']);
            i++;
        }

    }
    return final
};

//return links
export const tableHeaderLinks = (table)=>{
    let tables_ = tables;
    let arr = []; // array of tableView fields (dbName and title)
    let final = []; // final array to be returned (contain only dbName's)
    for (let key in tables_) {
        if (tables_.hasOwnProperty(key)) {
            if(table===key) {
                arr=(tables_[key]['tableView']);
            }
        }

    }
    let i=0;
    for (let key in arr) {
        if (arr.hasOwnProperty(key)) {
            if(arr[i]['link']) {
                final.push(arr[i]['link']);
                i++;
            }
        }

    }
    console.log("final",final);
    return final
};
export const searchColumns = (table)=>{
    let tables_ = tables;
    for (let key in tables_) {
        if (tables_.hasOwnProperty(key)) {
            if(table===key)
                return (tables_[key]['search']);
        }
    }
};

