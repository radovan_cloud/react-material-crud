import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { RoutedTabs, NavTab } from 'react-router-tabs';
import {HashRouter, Route, Switch,Redirect} from "react-router-dom";
import Master from "./containers/MastersPageWrapper";
import Wrap from "./containers/Wrapp";
import SignIn from "./pages/SignIn";
import { MuiThemeProvider,createMuiTheme } from '@material-ui/core/styles';
import {store,tableNameasArray} from "./config/masterSettings";
import './styles/react-router-tabs.css';
import './styles.css';
const theme = createMuiTheme({
    typography: {
        useNextVariants: true,
    },
});
const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={(props) => (
        store.entity.isLogged === true
            ? <Component {...props} />
            : <Redirect to='/' />
    )} />
);
const Tabovi = () => (
    <RoutedTabs
                 activeTabClassName="active">
        {store.Routerx.map((item)=>(
            item
        ))}
    </RoutedTabs>
);
ReactDOM.render(
    <MuiThemeProvider theme={theme}>
        <HashRouter>
            <div>
                {/*<Tabovi/>*/}
            {/*<RoutedTabs >*/}
                {/*/!*<NavTab to="/users">Admins</NavTab>*!/*/}
                {/*/!*<NavTab to="/posts">Moderators</NavTab>*!/*/}
                {/*{store.Routerx.map((item)=>(*/}
                    {/*item*/}
                {/*))}*/}
            {/*</RoutedTabs>*/}
            <Switch>
                <Route exact={true} path='/'
                       component={SignIn}/>
                {tableNameasArray().map((item,index)=>(
                    <PrivateRoute key={index} path={'/'+item} component={props => <Master ta={   <Tabovi/>} {...props} />}  />
                ))}
                <App />

            </Switch>

            </div>
        </HashRouter>
    </MuiThemeProvider>
    , document.getElementById('root'));
serviceWorker.unregister();
